all: build-demo build-paid

build-demo:
	./seagull.js --config demo.json build

build-paid:
	./seagull.js --config paid.json build

.PHONY: build-demo build-paid
